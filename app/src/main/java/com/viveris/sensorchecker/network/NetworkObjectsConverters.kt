package com.viveris.sensorchecker.network

import com.viveris.sensorchecker.database.*

/**
 * These functions extends the Network objects to transform them in the respective Database object.
 * Since all of them are containers, it expands to a list of database objects.
 */

fun NetworkToursContainer.netToursAsDatabase(): List<DatabaseTour> = tours.map { DatabaseTour(idTour = it.tourId, tourDate = it.date) }

fun NetworkActionsContainer.netActionsAsDatabase(): List<DatabaseAction> = actions.map { DatabaseAction(idAction = it.actionId, actionLabel = it.label) }

fun NetworkMeasurementsContainer.netMeasurementsAsDatabase(): List<DatabaseMeasurementType> = measurements.map { DatabaseMeasurementType(idType = it.typeId, measurement = it.measurement, unit = it.unit) }

fun NetworkSensorsContainer.netSensorsAsDatabase(): List<DatabaseSensor> =
    sensors.map {
        DatabaseSensor(idSensor = it.sensorId,
            idMeasurementType = it.measurementId,
            reference = it.reference,
            description = it.description,
            supplier = it.supplier,
            idState = it.stateId,
            isModified = false) }

fun NetworkStatesContainer.netStatesAsDatabase(): List<DatabaseSensorState> = states.map { DatabaseSensorState(idState = it.stateId, stateCode = it.stateCode, stateDesc = it.label) }

fun NetworkTourSensorsContainer.netTourSensorsAsDatabase(): List<DatabaseTourSensor> =
    tourSensors.map { DatabaseTourSensor(idTourSensor = it.tourSensorId, idSensor = it.sensorId, idTour = it.tourId, sensorOrder = it.order) }

fun List<DatabaseIntervention>.databaseAsNetwork(): NetworkInterventionsContainer {
    return NetworkInterventionsContainer(interventions =
        this.map { NetworkIntervention(actionId = it.idAction, tourSensorId = it.idTourSensor, comments = it.comments, newSensorId = it.idNewSensor) })
}

fun List<DatabaseSensor>.asModifiedSensor(): List<NetworkSensorUpdate> = this.map { NetworkSensorUpdate(sensorId = it.idSensor, stateId = it.idState) }