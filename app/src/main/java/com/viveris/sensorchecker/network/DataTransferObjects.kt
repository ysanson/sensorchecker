package com.viveris.sensorchecker.network

import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class NetworkToursContainer(val tours: List<NetworkTour>)

@JsonClass(generateAdapter = true)
data class NetworkTour(
    val tourId: Long,
    val date: Date,
    val userId: Long,
    val isModel: Boolean?,
    val modelName: String?
)

@JsonClass(generateAdapter = true)
data class NetworkActionsContainer(val actions: List<NetworkActions>)

@JsonClass(generateAdapter = true)
data class NetworkActions(
    val actionId: Long,
    val label: String
)

@JsonClass(generateAdapter = true)
data class NetworkMeasurementsContainer(val measurements: List<NetworkMeasurements>)

@JsonClass(generateAdapter = true)
data class NetworkMeasurements(
    val typeId: Long,
    val measurement: String,
    val unit: String
)

@JsonClass(generateAdapter = true)
data class NetworkSensorsContainer(val sensors: List<NetworkSensor>)

@JsonClass(generateAdapter = true)
data class NetworkSensor(
    val sensorId: Long,
    val measurementId: Long,
    val reference: String,
    val description: String,
    val supplier: String,
    val model: String,
    val stateId: Long,
    val positionId: Long
)

@JsonClass(generateAdapter = true)
data class NetworkStatesContainer(val states: List<NetworkStates>)

@JsonClass(generateAdapter = true)
data class NetworkStates(
    val stateId: Long,
    val stateCode: String,
    val label: String
)

@JsonClass(generateAdapter = true)
data class NetworkTourSensorsContainer(val tourSensors: List<NetworkTourSensor>)

@JsonClass(generateAdapter = true)
data class NetworkTourSensor(
    val tourSensorId: Long,
    val tourId: Long,
    val sensorId: Long,
    val order: Int
)

@JsonClass(generateAdapter = true)
data class NetworkInterventionsContainer(val interventions: List<NetworkIntervention>)

@JsonClass(generateAdapter = true)
data class NetworkIntervention(
    val actionId: Long,
    val tourSensorId: Long,
    val comments: String,
    val newSensorId: Long?
)

@JsonClass(generateAdapter = true)
data class NetworkSensorUpdate(
    val sensorId: Long,
    val stateId: Long
)

@JsonClass(generateAdapter = true)
data class NetworkUserRequest(
    val username: String,
    val password: String
)

@JsonClass(generateAdapter = true)
data class NetworkUserResponse(
    val isAuthenticated: Boolean,
    val userId: Long,
    val firstname: String,
    val lastname: String,
    val isRondier: Boolean,
    val authToken: String,
    val refreshToken: String
)

@JsonClass(generateAdapter = true)
data class NetworkTokenRequest(
    val refreshToken: String
)

@JsonClass(generateAdapter = true)
data class NetworkTokenResponse(val token: String)