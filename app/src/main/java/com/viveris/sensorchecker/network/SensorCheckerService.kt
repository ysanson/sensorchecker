package com.viveris.sensorchecker.network

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

/**
 * Contains the requests to the WebService.
 */
interface SensorCheckerService {
    @GET("tour")
    fun getToursAsync(@Query("userId") userId: Long): Deferred<NetworkToursContainer>

    @GET("action")
    fun getActionsAsync(): Deferred<NetworkActionsContainer>

    @GET("measurement")
    fun getMeasurementsAsync(): Deferred<NetworkMeasurementsContainer>

    @GET("sensor")
    fun getSensorsAsync(): Deferred<NetworkSensorsContainer>

    @GET("state")
    fun getStatesAsync(): Deferred<NetworkStatesContainer>

    @GET("tourSensor")
    fun getTourSensorsAsync(): Deferred<NetworkTourSensorsContainer>

    @POST("intervention")
    fun postInterventionsAsync(@Body interventions: NetworkInterventionsContainer): Deferred<Response<Unit>>

    @PATCH("sensor/{id}")
    fun updateSensorAsync(
        @Path("id") sensorId: Long,
        @Body sensor: NetworkSensorUpdate
    ): Deferred<Response<Unit>?>

    @POST("login")
    fun authenticateAsync(@Body credentials: NetworkUserRequest): Deferred<NetworkUserResponse>

    @POST("token")
    fun refreshTokenAsync(@Body refreshToken: NetworkTokenRequest): Deferred<NetworkTokenResponse>
}