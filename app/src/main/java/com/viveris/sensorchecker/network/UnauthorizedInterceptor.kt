package com.viveris.sensorchecker.network

import okhttp3.Interceptor
import okhttp3.Response

class UnauthorizedInterceptor: Interceptor {
    private val forbiddenCode = 403
    private val unauthorizedCode = 401

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        if (response.code == forbiddenCode) {
            throw ForbiddenException()
        } else if(response.code == unauthorizedCode) {
            throw UnauthorizedException()
        }
        return response
    }
}