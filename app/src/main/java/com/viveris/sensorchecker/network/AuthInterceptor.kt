package com.viveris.sensorchecker.network

import com.auth0.android.jwt.JWT
import com.squareup.moshi.Moshi
import com.viveris.sensorchecker.repository.ITokenRepository
import com.viveris.sensorchecker.repository.TokenRepository
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber

/**
 * Intercept every request and adds an authorization bearer token if needed.
 * If the token is expired, tries to replace it.
 */
class AuthInterceptor(private val tokenRepository: ITokenRepository, private val authUrl: String): Interceptor {
    private val json: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull();

    override fun intercept(chain: Interceptor.Chain): Response {
        val currentToken = tokenRepository.getAccessToken()
        val originalRequest = chain.request()
        return when {
            originalRequest.url.toString().contains("login") || originalRequest.url.toString().contains("token") -> chain.proceed(originalRequest)
            currentToken != null -> {
                val token = JWT(currentToken)
                if (!token.isExpired(10)) {
                    chain.proceed(originalRequest.newBuilder().addHeader("Authorization", "Bearer $currentToken").build())
                } else {
                    val refreshToken = tokenRepository.getRefreshToken()?: ""
                    val body = "{refreshToken: $refreshToken}"
                    val request = Request.Builder()
                        .url(authUrl)
                        .post(body.toRequestBody(json))
                        .build()
                    val refreshResponse = chain.proceed(request)
                    if(refreshResponse.isSuccessful) {
                        val newAccessToken = Moshi.Builder().build()
                            .adapter(NetworkTokenResponse::class.java).fromJson(refreshResponse.body.toString())
                        tokenRepository.replaceAccessToken(newAccessToken?.token?:"")
                        chain.proceed(originalRequest.newBuilder().addHeader("Authorization", "Bearer ${newAccessToken?.token}").build())
                    } else {
                        throw ForbiddenException()
                    }
                }
            }
            else -> throw UnauthorizedException()
        }
    }

}