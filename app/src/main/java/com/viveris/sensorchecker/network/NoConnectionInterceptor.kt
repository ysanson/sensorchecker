package com.viveris.sensorchecker.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.viveris.sensorchecker.SensorCheckerApplication
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class NoConnectionInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return if (!isConnectionOn()) {
            throw NoConnectivityException()
        } else if(!isInternetAvailable()) {
            throw NoInternetException()
        } else {
            chain.proceed(chain.request())
        }
    }

    /**
     * Checks if either the Wifi or Cellular is on.
     */
    private fun isConnectionOn(): Boolean {
        val connectivityManager = SensorCheckerApplication.applicationContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val connection = connectivityManager.getNetworkCapabilities(network)

        return connection != null && (
                connection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        connection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
    }

    /**
     * Checks if the Webservice responds.
     */
    private fun isInternetAvailable(): Boolean {
        return try {
            val timeoutMs = 1500
            val sock = Socket()
            val sockAddr = InetSocketAddress("8.8.8.8", 53)
            sock.connect(sockAddr, timeoutMs)
            sock.close()
            true
        } catch (e: IOException) {
            false
        }
    }
}

class NoConnectivityException : IOException() {
    override val message: String
        get() =
            "No network available, please check your WiFi or Data connection"
}

class NoInternetException() : IOException() {
    override val message: String
        get() =
            "The service is unavailable"
}