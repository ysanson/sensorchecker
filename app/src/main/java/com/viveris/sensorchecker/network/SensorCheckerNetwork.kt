package com.viveris.sensorchecker.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.viveris.sensorchecker.repository.TokenRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Encompasses the retrofit service.
 * The calls are in the interface [SensorCheckerService].
 */
object SensorCheckerNetwork {
    private const val BASE_URL: String = "https://sensor-check-mock.herokuapp.com"

    /**
     * An interceptor which logs the HTTP request and response.
     */
    private val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    /**
     * The client used for the network.
     * Here, more interceptors can be added, to catch HTTP error codes for example.
     */
    private val client = OkHttpClient.Builder()
        .connectTimeout(100, TimeUnit.SECONDS)
        .callTimeout(100, TimeUnit.SECONDS)
        .addInterceptor(loggingInterceptor)
        .addInterceptor(NoConnectionInterceptor())
        .addInterceptor(AuthInterceptor(TokenRepository(), "$BASE_URL/token"))
        .addInterceptor(UnauthorizedInterceptor())
        .build()

    /**
     * Moshi object.
     * The Date adapter is added, to help with the Date parsing.
     */
    private val moshiBuilder = Moshi.Builder()
        .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())

    /**
     * The retrofit instance.
     * Configure retrofit to parse JSON and use coroutines
     */
    private val retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshiBuilder.build()))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    /**
     * The service, called by the repositories when they want to interact with the server.
     */
    val service: SensorCheckerService = retrofit.create(SensorCheckerService::class.java)
}