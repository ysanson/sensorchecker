package com.viveris.sensorchecker.network

import java.io.IOException
import java.lang.Exception

class ForbiddenException: IOException() {
    override val message: String?
        get() = "Forbidden to access resource."
}

class UnauthorizedException: IOException() {
    override val message: String?
        get() = "Unauthorized. Bad credentials or token?"
}