package com.viveris.sensorchecker.domain

import com.viveris.sensorchecker.utils.toSimpleString
import java.util.*

/**
 * Domain objects are plain Kotlin data classes that represent the things in our app. These are the
 * objects that should be displayed on screen, or manipulated by the app.
 *
 * @see com.viveris.sensorchecker.database for objects that are mapped to the database
 * @see com.viveris.sensorchecker.network for objects that parse or prepare network calls
 */


data class User (val idUser: Long, val firstName: String, val lastName: String)

data class Tour (val idTour: Long, val date: Date) {
    val description: String
        get() = "Tour n°$idTour at ${date.toSimpleString()}."
}

data class MeasurementType(val idType: Long, val measurement: String, val unit: String) {
    val description: String
        get() = "$measurement ($unit)"
}

data class Action(val idAction: Long, val description: String) {
    override fun toString() = description
}

data class SensorState(val idState: Long, val code: String, val description: String) {
    val longDescription: String
        get() = "State: $description"

    override fun toString() = "($code) $description"
}

data class Sensor(val idSensor: Long,
                  val idMeasurementType: Long,
                  val reference: String,
                  val description:String,
                  val supplier: String,
                  val idState: Long,
                  var isModified: Boolean) {
    val longDescription: String
        get() = "Manufactured by $supplier (ref: $reference)\n$description"

    override fun toString(): String {
        return "Ref: $reference | $description"
    }
}

data class Intervention(val idIntervention: Long,
                        val idAction: Long,
                        val idTourSensor: Long,
                        val comments: String,
                        val idNewSensor: Long?  ) {
    val description: String
            get() = "ID: $idIntervention | ID Action: $idAction | On tour sensor $idTourSensor"
}

data class TourSensor(val tourSensorId: Long, val tourId: Long, val sensorId: Long, val sensorOrder: Int)

