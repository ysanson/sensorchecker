package com.viveris.sensorchecker.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {
    @Query("select * from utilisateur limit 1")
    fun getUser(): DatabaseUser?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun replaceUser(user: DatabaseUser)
    @Query("delete from utilisateur")
    fun deleteUsers()
}

@Dao
interface ToursDAO {
    @Query("select * from ronde")
    fun getTours(): LiveData<List<DatabaseTour>>
    @Query("select * from ronde where idTour = :idTour")
    fun getTourById(idTour: Long): DatabaseTour?
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(tours: List<DatabaseTour>)
    @Query("delete from ronde where idTour=:tourId")
    fun deleteTourById(tourId: Long)
    @Query("delete from ronde")
    fun deleteTours()
}

@Dao
interface MeasurementTypeDao {
    @Query("select * from typemesure")
    fun getTypes(): LiveData<List<DatabaseMeasurementType>>
    @Query("select * from typemesure where idType = :typeId")
    fun getTypeById(typeId: Long): DatabaseMeasurementType?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(types: List<DatabaseMeasurementType>)
    @Query("delete from typemesure")
    fun deleteTypes()
}

@Dao
interface ActionDao {
    @Query("select * from `action`")
    fun getActions(): LiveData<List<DatabaseAction>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(actions: List<DatabaseAction>)
    @Query("delete from `action`")
    fun deleteActions()
}

@Dao
interface SensorStateDao {
    @Query("select * from etatcapteur order by idState ASC")
    fun getStates(): LiveData<List<DatabaseSensorState>>
    @Query("select * from etatcapteur where idState = :stateId")
    fun getStateById(stateId: Long): DatabaseSensorState?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(states: List<DatabaseSensorState>)
    @Query("delete from etatcapteur")
    fun deleteStates()
}


@Dao
interface SensorDao {
    @Query("select * from capteur")
    fun getLiveSensors(): LiveData<List<DatabaseSensor>>
    @Query("select * from capteur")
    fun getSensors(): List<DatabaseSensor>?
    @Query("select * from capteur where idSensor = :sensorId")
    fun getSensorById(sensorId: Long): DatabaseSensor?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(sensors: List<DatabaseSensor>)
    @Query("select Capteur.idSensor, idMeasurementType, RefCapteur, description, fournisseur, idState, isModified from Capteur, RondeCapteur where Capteur.idSensor = RondeCapteur.idSensor and RondeCapteur.idTour = :tourId order by RondeCapteur.sensorOrder ASC")
    fun getAllSensorsFromTour(tourId: Long): List<DatabaseSensor>?
    @Query("select * from capteur where isModified = 1")
    fun getModifiedSensors(): List<DatabaseSensor>?
    @Query("select c.idSensor, idMeasurementType, RefCapteur, description, fournisseur, idState, isModified  from capteur c, rondecapteur rc where c.idSensor = rc.idSensor and rc.idTour = :tourId and c.isModified=1 ")
    fun getModifiedSensorsFromTour(tourId: Long): List<DatabaseSensor>?
    @Update
    fun updateSensor(sensor: DatabaseSensor)
    @Update
    fun updateSensors(sensors: List<DatabaseSensor>)
    @Query("delete from capteur")
    fun deleteAll()
}

@Dao
interface TourSensorDao {
    @Query("select * from rondecapteur")
    fun getTourSensors(): LiveData<List<DatabaseTourSensor>>
    @Query("select * from rondecapteur where idSensor = :sensorId and idTour = :tourId")
    fun getSpecificTourSensor(tourId: Long, sensorId: Long): DatabaseTourSensor?
    @Query("select count(*) from rondecapteur where idTour=:tourId")
    fun getNumberOfSensorsForTour(tourId: Long): Int
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(tourSensors: List<DatabaseTourSensor>)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(tourSensors: List<DatabaseTourSensor>)
    @Query("delete from rondecapteur where idTour=:tourId")
    fun deleteTourSensorsFromTour(tourId: Long)
    @Query("delete from rondecapteur")
    fun deleteTourSensors()
}

@Dao
interface InterventionDao {
    @Query("select * from intervention")
    fun getInterventions(): LiveData<List<DatabaseIntervention>>
    @Query("select * from intervention")
    fun getInterventionsToSend(): List<DatabaseIntervention>?
    @Query("select idIntervention from intervention order by idIntervention DESC limit 1")
    fun getHighestInterventionId(): Long?
    @Query("select idIntervention, idAction, i.idTourSensor, commentaire, idNouveauCapteur from Intervention i, rondecapteur rc where i.idTourSensor=rc.idTourSensor and rc.idTour=:tourId")
    fun getInterventionsFromTour(tourId: Long): List<DatabaseIntervention>?
    @Query("select count(idIntervention) from Intervention i, rondecapteur rc, ronde r where i.idTourSensor=rc.idTourSensor and rc.idTour=r.idTour = r.idTour=:tourId")
    fun getNumberOfInterventionsForTour(tourId: Long): Int
    @Query("select * from intervention where idTourSensor = :tourSensorId")
    fun getInterventionForTourSensor(tourSensorId: Long): DatabaseIntervention?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(interventions: List<DatabaseIntervention>)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(intervention: DatabaseIntervention)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(interventions: List<DatabaseIntervention>)
    @Query("delete from intervention where idIntervention=:interventionId")
    fun deleteInterventionById(interventionId: Long)
    @Query("delete from intervention")
    fun deleteInterventions()
}

