package com.viveris.sensorchecker.database

import androidx.room.*
import java.util.*

/**
 * This file contains all the tables used by the application.
 * It is used by Room, and populated by the network when available.
 * Every table is named DatabaseXXX to avoid confusion with other objects,  from the network or from the repository.
 * It's different from the schema used by the web application because it doesn't need every table.
 */

/**
 * The user data table.
 */
@Entity( tableName = "Utilisateur" )
data class DatabaseUser constructor(
    @PrimaryKey
    val idUser: Long,
    val firstName: String,
    val lastName: String
)

/**
 *
 * The tours data table.
 */
@Entity( tableName = "Ronde" )
data class DatabaseTour constructor(
    @PrimaryKey
    val idTour: Long,
    @ColumnInfo(name = "dateRonde")
    val tourDate: Date
)

/**
 * The sensor types data table.
 */
@Entity( tableName = "TypeMesure")
data class DatabaseMeasurementType constructor(
    @PrimaryKey
    val idType: Long,
    @ColumnInfo(name = "Grandeur")
    val measurement: String,
    @ColumnInfo(name = "unite")
    val unit: String
)

@Entity(tableName = "Action")
data class DatabaseAction constructor(
    @PrimaryKey
    val idAction: Long,
    @ColumnInfo(name = "LibelleAction")
    val actionLabel: String
)

@Entity(tableName = "EtatCapteur")
data class DatabaseSensorState constructor(
    @PrimaryKey
    val idState: Long,
    @ColumnInfo(name = "CodeEtat")
    val stateCode: String,
    @ColumnInfo(name = "LibelleEtat")
    val stateDesc: String
)

/**
 * The sensors data table.
 */
@Entity( tableName = "Capteur",
    foreignKeys = [ForeignKey(entity = DatabaseMeasurementType::class,
        parentColumns = ["idType"], childColumns = ["idMeasurementType"],
        onDelete = ForeignKey.NO_ACTION, onUpdate = ForeignKey.NO_ACTION),
        ForeignKey(entity = DatabaseSensorState::class,
        parentColumns = ["idState"], childColumns = ["idState"],
        onDelete = ForeignKey.NO_ACTION, onUpdate = ForeignKey.NO_ACTION)])
data class DatabaseSensor constructor(
    @PrimaryKey
    val idSensor: Long,
    @ColumnInfo(index = true)
    val idMeasurementType: Long,
    @ColumnInfo(name = "RefCapteur")
    val reference: String,
    val description:String,
    @ColumnInfo(name = "fournisseur")
    val supplier: String,
    @ColumnInfo(index = true)
    val idState: Long,
    val isModified: Boolean
)

@Entity(tableName = "RondeCapteur",
    foreignKeys = [ForeignKey(entity = DatabaseTour::class,
    parentColumns = ["idTour"], childColumns = ["idTour"],
    onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE),
    ForeignKey(entity = DatabaseSensor::class,
    parentColumns = ["idSensor"], childColumns = ["idSensor"],
    onDelete = ForeignKey.NO_ACTION, onUpdate = ForeignKey.NO_ACTION)])
data class DatabaseTourSensor constructor(
    @PrimaryKey
    val idTourSensor: Long,
    @ColumnInfo(index = true)
    val idTour: Long,
    @ColumnInfo(index = true)
    val idSensor: Long,
    val sensorOrder: Int
)

/**
 * The intervention data table.
 */
@Entity(tableName = "Intervention",
    foreignKeys = [ForeignKey(entity = DatabaseAction::class,
        parentColumns = ["idAction"], childColumns = ["idAction"],
        onDelete = ForeignKey.NO_ACTION, onUpdate = ForeignKey.NO_ACTION),
        ForeignKey(entity = DatabaseTourSensor::class,
        parentColumns = ["idTourSensor"], childColumns = ["idTourSensor"],
        onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE)])
data class DatabaseIntervention constructor(
    @PrimaryKey
    val idIntervention: Long,
    @ColumnInfo(index = true)
    val idAction: Long,
    @ColumnInfo(index = true)
    val idTourSensor: Long,
    @ColumnInfo(name = "commentaire")
    val comments: String,
    @ColumnInfo(name = "idNouveauCapteur")
    val idNewSensor: Long?
)
