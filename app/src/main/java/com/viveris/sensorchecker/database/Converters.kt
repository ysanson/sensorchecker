package com.viveris.sensorchecker.database

import androidx.room.TypeConverter
import com.viveris.sensorchecker.domain.*
import java.util.*

/**
 * Converts Java types into SQLite-compliant types.
 */
class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?) = value?.let { Date(it) }

    @TypeConverter
    fun dateToTimestamp(date: Date?) = date?.time
}

/**
 * Maps the user into a domain model.
 */
fun DatabaseUser?.asDomainModel(): User? {
    return if(this != null)
        User(idUser, firstName, lastName)
    else null
}

fun List<DatabaseTour>.tourAsDM(): List<Tour> {
    return map {
        Tour(idTour = it.idTour,
            date = it.tourDate)
    }
}

fun DatabaseTour?.asDomainModel(): Tour?{
    return if(this != null)
        Tour(idTour, date = this.tourDate)
    else null
}

fun List<DatabaseMeasurementType>.measurementTypeAsDM(): List<MeasurementType> {
    return map {
        MeasurementType(idType = it.idType, measurement = it.measurement, unit = it.unit)
    }
}

fun DatabaseMeasurementType?.asDomainModel(): MeasurementType? {
    return if(this != null)
        MeasurementType(idType = this.idType, measurement = this.measurement, unit = this.unit)
    else null
}

fun List<DatabaseAction>.actionAsDM(): List<Action> {
    return map {
        Action(idAction = it.idAction, description = it.actionLabel)
    }
}

fun List<DatabaseSensorState>.sensorStateAsDM(): List<SensorState> {
    return map {
        SensorState(idState = it.idState, code = it.stateCode, description = it.stateDesc)
    }
}

fun DatabaseSensorState?.asDomainModel(): SensorState? {
    return if (this != null)
        SensorState(idState = this.idState, code = this.stateCode, description = this.stateDesc)
    else null
}

fun List<DatabaseSensor>.sensorAsDM(): List<Sensor> {
    return map {
        Sensor(idSensor = it.idSensor,
            idMeasurementType = it.idMeasurementType,
            reference = it.reference,
            description = it.description,
            supplier = it.supplier,
            idState = it.idState,
            isModified = it.isModified)
    }
}

fun DatabaseSensor?.asDomainModel(): Sensor? {
    return if(this != null)
        Sensor(idSensor, idMeasurementType, reference, description, supplier, idState, isModified)
    else null
}

fun List<DatabaseIntervention>.interventionAsDM(): List<Intervention> {
    return map {
        Intervention(idIntervention = it.idIntervention,
            idAction = it.idAction,
            idTourSensor = it.idTourSensor,
            comments = it.comments,
            idNewSensor = it.idNewSensor)
    }
}

fun DatabaseTourSensor?.asDomainModel(): TourSensor? {
    return if(this != null)
        TourSensor(this.idTourSensor, this.idTour, this.idSensor, this.sensorOrder)
    else null
}