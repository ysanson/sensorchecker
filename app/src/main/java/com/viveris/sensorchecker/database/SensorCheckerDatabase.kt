package com.viveris.sensorchecker.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [DatabaseUser::class,
    DatabaseTour::class,
    DatabaseMeasurementType::class,
    DatabaseAction::class,
    DatabaseSensorState::class,
    DatabaseSensor::class,
    DatabaseTourSensor::class,
    DatabaseIntervention::class],
    version = 10,
    exportSchema = false)
@TypeConverters(Converters::class)
abstract class SensorCheckerDatabase : RoomDatabase() {
    abstract val userDao: UserDao
    abstract val toursDao: ToursDAO
    abstract val measurementTypeDao: MeasurementTypeDao
    abstract val actionDao: ActionDao
    abstract val sensorStateDao: SensorStateDao
    abstract val sensorDao: SensorDao
    abstract val tourSensorDao: TourSensorDao
    abstract val interventionDao: InterventionDao

    companion object {
        @Volatile
        private var INSTANCE: SensorCheckerDatabase? = null

        /**
         * Gets the database, or creates it if it's not already created.
         * Thread-safe.
         * @param context The application context Singleton, used to get access to the filesystem.
         */
        fun getInstance(context: Context): SensorCheckerDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if(instance == null) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        SensorCheckerDatabase::class.java,
                        "sensor_checker_database")
                        //Wipes and rebuilds the database instead of migrating.
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}