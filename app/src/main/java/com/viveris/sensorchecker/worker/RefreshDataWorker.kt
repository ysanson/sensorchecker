package com.viveris.sensorchecker.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.viveris.sensorchecker.database.SensorCheckerDatabase.Companion.getInstance
import com.viveris.sensorchecker.repository.*
import java.lang.Exception

/**
 * The refresh data worker, refreshes avery table in the cache.
 */
class RefreshDataWorker(appContext: Context, params: WorkerParameters): CoroutineWorker(appContext, params) {
    companion object {
        const val WORK_NAME = "com.viveris.sensorchecker.worker.RefreshDataWorker"
    }

    override suspend fun doWork(): Result {
        val database = getInstance(applicationContext)
        try {
            ActionRepository(database).retrieveActions()
            MeasurementTypeRepository(database).refreshMeasurements()
            SensorStateRepository(database).refreshSensorStates()
            TourRepository(database).refreshTours()
            SensorRepository(database).refreshSensors()
        } catch (e: Exception) {
            return Result.failure()
        }
        return Result.success()
    }
}