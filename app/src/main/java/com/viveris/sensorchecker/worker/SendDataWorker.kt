package com.viveris.sensorchecker.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.repository.InterventionRepository
import com.viveris.sensorchecker.repository.SensorRepository
import timber.log.Timber
import java.lang.Exception

/**
 * Sends data in the background.
 */
class SendDataWorker(appContext: Context, params: WorkerParameters): CoroutineWorker(appContext, params) {
    companion object {
        const val WORK_NAME = "com.viveris.sensorchecker.worker.SendDataWorker"
    }

    override suspend fun doWork(): Result {
        val database = SensorCheckerDatabase.getInstance(applicationContext)
        val interventionRepository = InterventionRepository(database)
        val sensorRepository = SensorRepository(database)
        try {
            interventionRepository.sendNewInterventions()
            sensorRepository.sendUpdatedSensors()
        } catch (e: Exception) {
            return Result.failure()
        }
        return Result.success()
    }
}