package com.viveris.sensorchecker

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import timber.log.Timber
import androidx.work.*
import com.viveris.sensorchecker.worker.RefreshDataWorker
import com.viveris.sensorchecker.worker.SendDataWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit
import com.viveris.sensorchecker.database.SensorCheckerDatabase.Companion.getInstance
import com.viveris.sensorchecker.repository.*
import java.lang.Exception

/**
 * The main entry point of the application.
 * Used to setup background tasks on startup for example.
 */
class SensorCheckerApplication: Application() {

    private val applicationScope = CoroutineScope(Dispatchers.Default)

    override fun onCreate() {
        super.onCreate()
        instance = this
        sharedPreferences = getSharedPreferences("AppSettings", Context.MODE_PRIVATE)
        //We need this line in order to instantiate the DB, otherwise it will return weird results
        getInstance(this)

        delayedInit()
    }

    /**
     * Handles all the stuff at startup that would hand on the main thread
     */
    private fun delayedInit() {
        applicationScope.launch {
            Timber.plant(Timber.DebugTree())
            //refreshCache()
            setupRefreshWorker()
            setupSenderWorker()
        }
    }

    /**
     * Sets up a refresh cache worker, to perform an automatic refresh.
     */
    private suspend fun refreshCache() {
        val database = getInstance(this@SensorCheckerApplication.applicationContext)
        try {
            ActionRepository(database).retrieveActions()
            MeasurementTypeRepository(database).refreshMeasurements()
            SensorStateRepository(database).refreshSensorStates()
            TourRepository(database).refreshTours()
            SensorRepository(database).refreshSensors()
        } catch (e: Exception) {
            Timber.d("Exception occurred! ${e.fillInStackTrace()}")
        }
    }

    /**
     * Setups the refresh worker, so that the cache is up to date in case the network is down when the app is open. It works only if:
     * - The network is available
     * - The device is charging
     * - The battery is not low
     * - The device is not used.
     */
    private fun setupRefreshWorker() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .setRequiresCharging(true)
            .setRequiresBatteryNotLow(true)
            .setRequiresDeviceIdle(true)
            .build()

        val repeatingRequest = PeriodicWorkRequestBuilder<RefreshDataWorker>(1, TimeUnit.DAYS)
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance().enqueueUniquePeriodicWork(
            RefreshDataWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            repeatingRequest)
    }

    /**
     * Setups the worker to send the new interventions and the updated sensors, if any.
     */
    private fun setupSenderWorker() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .setRequiresBatteryNotLow(true)
            .setRequiresDeviceIdle(true)
            .build()

        val repeatingRequest = PeriodicWorkRequestBuilder<SendDataWorker>(1, TimeUnit.DAYS)
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance().enqueueUniquePeriodicWork(
            SendDataWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.REPLACE,
            repeatingRequest
        )
    }

    /**
     * The companion object is used to provide static methods to the app.
     */
    companion object {
        private lateinit var instance: SensorCheckerApplication
        private lateinit var sharedPreferences: SharedPreferences

        /**
         * Gets the app context.
         * Useful if we need to access it outside of fragments.
         */
        fun applicationContext() : Context {
            return instance.applicationContext
        }

        /**
         * Adds a setting to the sharedPreferences dictionary.
         * @param name the name of the setting
         * @param value the valmue of the setting
         */
        fun addSetting(name: String, value: String) {
            val editor = sharedPreferences.edit()
            editor.putString(name, value)
            editor.apply()
        }

        /**
         * Removes a setting in the preferences by name.
         * @param name The param to remove
         */
        fun removeSetting(name: String) {
            val editor = sharedPreferences.edit()
            editor.remove(name)
            editor.apply()
        }

        /**
         * Gets a setting in the preferences by its name.
         * @param name the param name.
         * @return the value if present, null otherwise.
         */
        fun getSetting(name: String): String? {
            return if(sharedPreferences.contains(name)) {
                sharedPreferences.getString(name, null)
            } else null
        }
    }
}