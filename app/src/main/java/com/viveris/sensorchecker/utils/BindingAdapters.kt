package com.viveris.sensorchecker.utils

import android.view.View
import androidx.databinding.BindingAdapter

/**
 * Binding adapter used to hide the spinner once data is available.
 */
@BindingAdapter("isNetworkError", "list")
fun hideIfNetworkError(view: View, isNetWorkError: Boolean, list: Any?) {
    view.visibility = if (list != null) View.GONE else View.VISIBLE

    if(isNetWorkError) {
        view.visibility = View.GONE
    }
}