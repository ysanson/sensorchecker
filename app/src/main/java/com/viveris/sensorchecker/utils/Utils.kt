package com.viveris.sensorchecker.utils

import android.app.Activity
import android.content.Context
import android.os.IBinder
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.viveris.sensorchecker.SensorCheckerApplication
import com.viveris.sensorchecker.network.NetworkTokenRequest
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.UnauthorizedException
import com.viveris.sensorchecker.repository.TokenRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

/**
 * Hides the keyboard from the view.
 */
fun hideKeyboard(view: View) {
    // Retrieving the token if the view is hosted by the fragment.
    var windowToken: IBinder? = view.windowToken

    // Retrieving the token if the view is hosted by the activity.
    if (windowToken == null) {
        if (view.context is Activity) {
            val activity = view.context as Activity
            if (activity.window != null) {
                windowToken = activity.window.decorView.windowToken
            }
        }
    }

    // Hide if shown before.
    val inputMethodManager = view
        .context
        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)

    view.clearFocus()
}

/**
 * Adds a method to the date class to add a simple date format.
 */
fun Date.toSimpleString(): String {
    val format = SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE)
    return format.format(this)
}

/**
 * Tries to refresh the access token using the refresh token.
 */
suspend fun refreshTokens() {
    withContext(Dispatchers.IO) {
        val refreshToken = SensorCheckerApplication.getSetting("RefreshToken")
        if(refreshToken != null) {
            val tokenRepository = TokenRepository()
            val response = SensorCheckerNetwork.service.refreshTokenAsync(NetworkTokenRequest(refreshToken)).await()
            tokenRepository.replaceAccessToken(response.token)
        } else {
            throw UnauthorizedException()
        }
    }
}


class NotTourManException: Exception() {
    override val message: String?
        get() = "This user does not do tours!"
}

class NoRegisteredUserException: Exception() {
    override val message: String?
        get() = "The user is not present in the database"
}