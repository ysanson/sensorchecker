package com.viveris.sensorchecker.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.viveris.sensorchecker.database.*
import com.viveris.sensorchecker.domain.Sensor
import com.viveris.sensorchecker.domain.TourSensor
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.asModifiedSensor
import com.viveris.sensorchecker.network.netSensorsAsDatabase
import com.viveris.sensorchecker.network.netTourSensorsAsDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Manages the sensors, by presenting a unified access from the application.
 *
 */
class SensorRepository(private val database: SensorCheckerDatabase) {
    //The sensors stored in the DB
    val sensors: LiveData<List<Sensor>> = Transformations.map(database.sensorDao.getLiveSensors()){ it.sensorAsDM() }

    /**
     * Gets the sensors from a specific tour
     * @param tourId The tour ID
     * @return The list of sensors associated with this specific tour.
     */
    suspend fun getSensorsFromTour(tourId: Long): List<Sensor>{
        return withContext(Dispatchers.IO) {
            database.sensorDao.getAllSensorsFromTour(tourId)?.sensorAsDM() ?: emptyList()
        }
    }

    /**
     * Gets the corresponding tour sensor in the database.
     * @param tourId the tour to search for
     * @param sensorId the sensor to search for
     * @return The tour sensor if found, null otherwise.
     */
    suspend fun getTourSensor(tourId: Long?, sensorId: Long?): TourSensor? {
        if(tourId==null || sensorId == null) return null
        return withContext(Dispatchers.IO) {
            database.tourSensorDao.getSpecificTourSensor(tourId, sensorId).asDomainModel()
        }
    }

    /**
     * Gets a sensor by its ID.
     * @param sensorId the id of the sensor.
     * @return the sensor if found, null otherwise.
     */
    suspend fun getSensorById(sensorId: Long): Sensor? {
        return withContext(Dispatchers.IO) {
            database.sensorDao.getSensorById(sensorId).asDomainModel()
        }
    }

    /**
     * Updates a sensor in the database with the new information.
     * @param sensor the updated Sensor to insert.
     */
    suspend fun updateSensor(sensor: DatabaseSensor) {
        withContext(Dispatchers.IO) {
            database.sensorDao.updateSensor(sensor)
        }
    }


    /**
     * Creates a new sensor that's the exact copy of the existing sensor, but marks it as modified.
     * Used when no actions are performed on it.
     * @param sensorId the sensor to mark modified.
     */
    suspend fun updateSensorWithNothing(sensorId: Long) {
        withContext(Dispatchers.IO) {
            val currentSensor = database.sensorDao.getSensorById(sensorId)
            if (currentSensor != null) {
                val newSensor = DatabaseSensor(
                    currentSensor.idSensor,
                    currentSensor.idMeasurementType,
                    currentSensor.reference,
                    currentSensor.description,
                    currentSensor.supplier,
                    currentSensor.idState,
                    true
                )
                database.sensorDao.updateSensor(newSensor)
            }
        }
    }

    /**
     * Returns if whether the sensor is modified or not.
     * If it doesn't exist, returns false.
     * @param sensorId the sensor to look for.
     * @return the modified state of the sensor, or false if not found.
     */
    suspend fun isSensorUpdated(sensorId: Long): Boolean {
        return withContext(Dispatchers.IO) {
            database.sensorDao.getSensorById(sensorId)?.isModified ?: false
        }
    }

    /**
     * Sends all the updated sensors for a specific tour.
     * @param tourId the tour to send the sensors for.
     * @return the number of sensors sent.
     */
    suspend fun sendUpdatedSensorsFromTour(tourId: Long): Int{
        return withContext(Dispatchers.IO) {
            val sensors = database.sensorDao.getModifiedSensorsFromTour(tourId)
            send(sensors)
        }
    }

    /**
     * Sends the updated sensors to the server.
     * @return the number of sensors sent.
     */
    suspend fun sendUpdatedSensors(): Int {
        return withContext(Dispatchers.IO) {
            val sensors = database.sensorDao.getModifiedSensors()
            send(sensors)
        }
    }

    /**
     * Handles the network connection to send to the sensors.
     * Then proceeds to mark the sensors as "not modified".
     * @return the number of sensors sent
     * @throws Exception if the network in unreachable, or if retrofit fails.
     */
    private suspend fun send(sensorsList: List<DatabaseSensor>?): Int {
        return withContext(Dispatchers.IO) {
            if(!sensorsList.isNullOrEmpty()) {
                sensorsList.asModifiedSensor().forEach {
                    SensorCheckerNetwork.service.updateSensorAsync(it.sensorId, it).await()
                }
                val newSensors = sensorsList.map { DatabaseSensor(it.idSensor, it.idMeasurementType, it.reference, it.description, it.supplier, it.idState, false) }
                database.sensorDao.updateSensors(newSensors)
                sensorsList.size
            } else 0
        }
    }

    /**
     * Refreshes the local DB with what is pulled from the Internet.
     * Avoids updating the locally modified sensors.
     */
    suspend fun refreshSensors() {
        withContext(Dispatchers.IO) {
            val modifiedSensors = database.sensorDao.getModifiedSensors()
            val newSensors = SensorCheckerNetwork.service.getSensorsAsync().await().netSensorsAsDatabase()
            val sensorsToAdd: List<DatabaseSensor> =
                if(modifiedSensors?.isNullOrEmpty() == true) newSensors
                else {
                    val modifiedIds: List<Pair<Long, Boolean>> = modifiedSensors.map { Pair(it.idSensor, it.isModified) }
                    newSensors.filterNot { modifiedIds.contains(Pair(it.idSensor, true)) }
                }
            val tourSensors = SensorCheckerNetwork.service.getTourSensorsAsync().await()
            database.sensorDao.insertAll(sensorsToAdd)
            database.tourSensorDao.insertAll(tourSensors.netTourSensorsAsDatabase())
        }
    }
}