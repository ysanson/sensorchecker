package com.viveris.sensorchecker.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.viveris.sensorchecker.database.DatabaseIntervention
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.database.interventionAsDM
import com.viveris.sensorchecker.domain.Intervention
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.databaseAsNetwork
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * The intervention repository
 */
class InterventionRepository(private val database: SensorCheckerDatabase) {
    val interventions: LiveData<List<Intervention>> = Transformations.map(database.interventionDao.getInterventions()) { it.interventionAsDM() }

    /**
     * Gets the next ID to store a new intervention.
     */
    suspend fun getNextInterventionId(): Long {
        return withContext(Dispatchers.IO) {
            database.interventionDao.getHighestInterventionId()?.plus(1L) ?: 1L
        }
    }

    /**
     * Inserts a new intervention in the database.
     * If an intervention for this tourSensor has already been done, it replaces it.
     * @param intervention the intervention to insert
     */
    suspend fun insertNewIntervention(intervention: DatabaseIntervention) {
        withContext(Dispatchers.IO) {
            val previousIntervention = database.interventionDao.getInterventionForTourSensor(intervention.idTourSensor)
            if (previousIntervention != null) {
                database.interventionDao.deleteInterventionById(previousIntervention.idIntervention)
            }
            database.interventionDao.insert(intervention)
        }
    }

    /**
     * Sends the interventions for a specific tour.
     * @param tourId the tour to send the interventions for.
     * @return The number of interventions sent.
     */
    suspend fun sendInterventionsFromTour(tourId: Long): Int {
        return withContext(Dispatchers.IO) {
            send(database.interventionDao.getInterventionsFromTour(tourId))
        }
    }

    /**
     * Sends new interventions to the server.
     * @return The number of interventions sent.
     */
    suspend fun sendNewInterventions(): Int {
        return withContext(Dispatchers.IO) {
            send(database.interventionDao.getInterventionsToSend())
        }
    }

    /**
     * Sends the list of interventions given in parameter to the web service.
     * @param interventionList the list of DatabaseIntervention to send.
     * @return The number of interventions sent.
     * @throws Exception if the user is forbidden to post the resources.
     */
    private suspend fun send(interventionList: List<DatabaseIntervention>?): Int {
        return withContext(Dispatchers.IO) {
            if(!interventionList.isNullOrEmpty()){
                SensorCheckerNetwork.service.postInterventionsAsync(interventionList.databaseAsNetwork()).await()
                database.interventionDao.deleteInterventions()
                interventionList.size
            } else 0
        }
    }
}