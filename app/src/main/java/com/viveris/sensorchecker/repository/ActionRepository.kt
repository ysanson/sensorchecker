package com.viveris.sensorchecker.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.database.actionAsDM
import com.viveris.sensorchecker.domain.Action
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.netActionsAsDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ActionRepository(private val database: SensorCheckerDatabase) {
    val actions: LiveData<List<Action>> = Transformations.map(database.actionDao.getActions()) { it.actionAsDM() }

    /**
     * Retrieves the actions from the database.
     */
    suspend fun retrieveActions() {
        withContext(Dispatchers.IO) {
            val actions = SensorCheckerNetwork.service.getActionsAsync().await()
            database.actionDao.insertAll(actions.netActionsAsDatabase())
        }
    }
}