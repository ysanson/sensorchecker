package com.viveris.sensorchecker.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.database.asDomainModel
import com.viveris.sensorchecker.database.tourAsDM
import com.viveris.sensorchecker.domain.Tour
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.netToursAsDatabase
import com.viveris.sensorchecker.utils.NoRegisteredUserException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class TourRepository(private val database: SensorCheckerDatabase) {
    val tours: LiveData<List<Tour>> = Transformations.map(database.toursDao.getTours()) { it.tourAsDM() }

    /**
     * Gets a specific tour from the database.
     * Runs asynchronously.
     * @param  tourId the tourId to search for.
     * @return an optional Tour, null if not present in the DB.
     */
    suspend fun getTourById(tourId: Long): Tour? {
        return withContext(Dispatchers.IO) {
            database.toursDao.getTourById(tourId).asDomainModel()
        }
    }

    /**
     * Checks if a tour is completed.
     * A tour is completed if every sensor in it has an intervention to it, even if it's "nothing to do" kind of thing
     * @param tourId: the tour to check
     * @return true if the tour is complete (if there is an intervention per sensor), false otherwise.
     */
    suspend fun isTourComplete(tourId: Long): Boolean {
        return withContext(Dispatchers.IO) {
            val interventionsCount = database.interventionDao.getNumberOfInterventionsForTour(tourId)
            val sensorsCount = database.tourSensorDao.getNumberOfSensorsForTour(tourId)
            interventionsCount == sensorsCount
        }
    }

    /**
     * Deletes a tour in the database, along with all the tourSensors and interventions in it.
     * @param tourId: the tour to delete.
     */
    suspend fun deleteTourById(tourId: Long) {
        withContext(Dispatchers.IO) {
            database.toursDao.deleteTourById(tourId)
        }
    }

    /**
     * Updates the tours.
     * Upon refreshing, updates only the tours that doesn't have interventions in them.
     */
    suspend fun refreshTours() {
        withContext(Dispatchers.IO) {
            val user = database.userDao.getUser()
            if (user != null) {
                val tours = SensorCheckerNetwork.service.getToursAsync(user.idUser)
                    .await()
                    .netToursAsDatabase()
                    .filterNot {
                        database.interventionDao.getNumberOfInterventionsForTour(it.idTour) > 0
                    }
                database.toursDao.insertAll(tours)
            } else throw NoRegisteredUserException()
        }
    }
}