package com.viveris.sensorchecker.repository

import com.viveris.sensorchecker.SensorCheckerApplication

/**
 * Defines the different methods of a token repository.
 * Used for test purposes, and mocking.
 */
interface ITokenRepository {
    fun getAccessToken(): String?
    fun replaceAccessToken(newToken: String)
    fun removeTokens()
    fun getRefreshToken(): String?
    fun setTokens(accessToken: String, refreshToken: String)
}

/**
 * Provides a clean interface to set and retrieve tokens from the shared preferences.
 */
class TokenRepository : ITokenRepository {

    /**
     * Retrieves the access token from the preferences.
     * @return the access token or null if not present.
     */
    override fun getAccessToken() = SensorCheckerApplication.getSetting("AccessToken")

    /**
     * Replaces the access token.
     * @param newToken the new JWT token (without the Bearer keyword before)
     */
    override fun replaceAccessToken(newToken: String) {
        SensorCheckerApplication.removeSetting("AccessToken")
        SensorCheckerApplication.addSetting("AccessToken", newToken)
    }

    /**
     * Removes all tokens in the preferences.
     */
    override fun removeTokens() {
        SensorCheckerApplication.removeSetting("AccessToken")
        SensorCheckerApplication.removeSetting("RefreshToken")
    }

    /**
     * Gets the refresh token.
     * @return the refresh token given by the WebService, or null if not present.
     */
    override fun getRefreshToken() = SensorCheckerApplication.getSetting("RefreshToken")

    /**
     * Sets both the access and refresh token. Typically used during login.
     * @param accessToken the given access token
     * @param refreshToken the given refresh token
     */
    override fun setTokens(accessToken: String, refreshToken: String) {
        SensorCheckerApplication.addSetting("AccessToken", accessToken)
        SensorCheckerApplication.addSetting("RefreshToken", refreshToken)
    }
}