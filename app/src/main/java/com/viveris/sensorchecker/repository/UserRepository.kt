package com.viveris.sensorchecker.repository

import com.viveris.sensorchecker.database.DatabaseUser
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.database.asDomainModel
import com.viveris.sensorchecker.domain.User
import com.viveris.sensorchecker.network.NetworkUserRequest
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.utils.NotTourManException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class UserRepository(private val database: SensorCheckerDatabase) {

    /**
     * Gets the current authenticated user.
     * @return The user or null if not set.
     */
    suspend fun getUser(): User? {
        return withContext(Dispatchers.IO) {
            database.userDao.getUser()?.asDomainModel()
        }
    }

    /**
     * Tries to authenticate the user with given username and passwords.
     * Stores a new user in the database, and the acces tokens and refresh tokens in the app settings.
     * @param username the provided username
     * @param password the provided password
     */
    suspend fun authenticate(username: String, password: String) {
        withContext(Dispatchers.IO) {
            try {
                val body = NetworkUserRequest(username, password)
                val response = SensorCheckerNetwork.service.authenticateAsync(body).await()
                if(response.isRondier) {
                    val newUser = DatabaseUser(idUser = response.userId, firstName = response.firstname, lastName = response.lastname)
                    database.userDao.deleteUsers()
                    database.userDao.replaceUser(newUser)
                    TokenRepository().setTokens(response.authToken, response.refreshToken)
                } else {
                 throw NotTourManException()
                }
            } catch (e: Exception) {
                throw e
            }
        }
    }


    /**
     * Deletes everything in the cache.
     */
    suspend fun logout() {
        withContext(Dispatchers.IO) {
            database.userDao.deleteUsers()
            database.toursDao.deleteTours()
            database.interventionDao.deleteInterventions()
            database.tourSensorDao.deleteTourSensors()
            database.sensorDao.deleteAll()
            database.measurementTypeDao.deleteTypes()
            database.actionDao.deleteActions()
            TokenRepository().removeTokens()
        }
    }
}