package com.viveris.sensorchecker.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.database.asDomainModel
import com.viveris.sensorchecker.database.sensorStateAsDM
import com.viveris.sensorchecker.domain.SensorState
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.netStatesAsDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SensorStateRepository(private val database: SensorCheckerDatabase) {
    val states: LiveData<List<SensorState>> = Transformations.map(database.sensorStateDao.getStates()) { it.sensorStateAsDM() }

    suspend fun getStateById(stateId: Long): SensorState? {
        return withContext(Dispatchers.IO) {
            database.sensorStateDao.getStateById(stateId).asDomainModel()
        }
    }
    /**
     * Refreshes the sensor states in the DB.
     */
    suspend fun refreshSensorStates() {
        withContext(Dispatchers.IO) {
            val state = SensorCheckerNetwork.service.getStatesAsync().await()
            database.sensorStateDao.insertAll(state.netStatesAsDatabase())
        }
    }
}