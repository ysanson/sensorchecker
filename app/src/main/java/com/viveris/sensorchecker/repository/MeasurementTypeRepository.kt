package com.viveris.sensorchecker.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.database.asDomainModel
import com.viveris.sensorchecker.database.measurementTypeAsDM
import com.viveris.sensorchecker.domain.MeasurementType
import com.viveris.sensorchecker.network.SensorCheckerNetwork
import com.viveris.sensorchecker.network.netMeasurementsAsDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Manages the measurements types.
 */
class MeasurementTypeRepository(private val database: SensorCheckerDatabase) {
    val measurementTypes: LiveData<List<MeasurementType>> = Transformations.map(database.measurementTypeDao.getTypes()) { it.measurementTypeAsDM() }

    suspend fun getMeasurementById(measurementId: Long): MeasurementType? {
        return withContext(Dispatchers.IO) {
            database.measurementTypeDao.getTypeById(measurementId).asDomainModel()
        }
    }
    /**
     * Refresh the measurements on the database.
     */
    suspend fun refreshMeasurements() {
        withContext(Dispatchers.IO) {
            val measurements = SensorCheckerNetwork.service.getMeasurementsAsync().await()
            database.measurementTypeDao.insertAll(measurements.netMeasurementsAsDatabase())
        }
    }

}