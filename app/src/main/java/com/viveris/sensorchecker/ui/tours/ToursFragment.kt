package com.viveris.sensorchecker.ui.tours

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.databinding.ToursFragmentBinding

class ToursFragment : Fragment() {

    /**
     * Lazy initialization of the viewModel, to ensure the activity is present during the lifecycle.
     */
    private val viewModel: ToursViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(this, ToursViewModelFactory(activity.application))
            .get(ToursViewModel::class.java)
    }

    /**
     * The adapter for the recycler view.
     */
    private var toursListAdapter: ToursListAdapter? = null

    /**
     * Creates the option menu, to provide some options, for example refreshing the content or disconnecting.
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.tour_list_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**
     * Defines the actions taken for the different menu options.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.menu_disconnect -> {
                viewModel.logout()
                this.findNavController().navigate(ToursFragmentDirections.actionToursFragmentToLoginFragment())
                true
            }
            R.id.menu_to_interventions -> {
                this.findNavController().navigate(ToursFragmentDirections.actionToursFragmentToInterventionListFragment())
                true
            }
            R.id.menu_send_all_changes -> {
                viewModel.sendPendingOperations()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Called when the fragment's activity has been created and this
     * fragment's view hierarchy instantiated.  It can be used to do final
     * initialization once these pieces are in place, such as retrieving
     * views or restoring state.
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.tours.observe(viewLifecycleOwner, Observer { tours ->
            toursListAdapter?.tours = tours
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = "Tours"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        val binding: ToursFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.tours_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.getUser()
        viewModel.user.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Snackbar.make(binding.root, "Hello ${it.firstName} ${it.lastName}", Snackbar.LENGTH_SHORT).show()
            }
        })

        toursListAdapter = ToursListAdapter(TourClick{
            this.findNavController().navigate(ToursFragmentDirections.actionToursFragmentToTourDetailsFragment(it.idTour))
        })

        //Implements the pull to refresh on the list.
        binding.swipeContainer.setOnRefreshListener { viewModel.refreshDataFromRepository() }

        viewModel.isRefreshed.observe(viewLifecycleOwner, Observer {
            if(it) {
                binding.swipeContainer.isRefreshing = false
                viewModel.onDoneRefreshed()
            }
        })

        binding.toursRecyclerView.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            layoutManager = LinearLayoutManager(context)
            adapter = toursListAdapter
            ItemTouchHelper(SwipeCallback(toursListAdapter!!, viewModel))
                .attachToRecyclerView(this)
        }

        // Observer for the network error.
        viewModel.showEvent.observe(viewLifecycleOwner, Observer { isEventShown ->
            if (isEventShown) onEventShown()
        })

        binding.toursViewModel = viewModel

        return binding.root
    }

    /**
     * Method for displaying a Toast error message for network errors.
     */
    private fun onEventShown() {
        if(!viewModel.isEventShown.value!!) {
            Toast.makeText(activity, viewModel.eventMessage, Toast.LENGTH_LONG).show()
            viewModel.onNetworkErrorShown()
        }
    }
}
