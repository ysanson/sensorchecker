package com.viveris.sensorchecker.ui.interventionList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.databinding.InterventionListItemBinding
import com.viveris.sensorchecker.domain.Intervention

class InterventionListAdapter(private val callback: InterventionClick): RecyclerView.Adapter<InterventionsViewHolder>() {
    var interventions: List<Intervention> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterventionsViewHolder {
        val withDataBinding: InterventionListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            InterventionsViewHolder.LAYOUT,
            parent,
            false)
        return InterventionsViewHolder(withDataBinding)
    }

    override fun getItemCount() = interventions.size

    override fun onBindViewHolder(holder: InterventionsViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.intervention = interventions[position]
            it.interventionCallback = callback
        }
    }
}


class InterventionsViewHolder(val viewDataBinding: InterventionListItemBinding): RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.intervention_list_item
    }
}

class InterventionClick(val block: (Intervention) -> Unit) {
    fun onClick(intervention: Intervention) = block(intervention)
}