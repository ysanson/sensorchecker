package com.viveris.sensorchecker.ui.tourDetails

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viveris.sensorchecker.database.DatabaseIntervention
import com.viveris.sensorchecker.database.SensorCheckerDatabase.Companion.getInstance
import com.viveris.sensorchecker.domain.Sensor
import com.viveris.sensorchecker.domain.Tour
import com.viveris.sensorchecker.repository.*
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.IOException

class TourDetailsViewModel(application: Application): ViewModel() {
    private val database = getInstance(application)

    private val tourRepository = TourRepository(database)
    val tours = tourRepository.tours

    private val sensorRepository = SensorRepository(database)
    private var _sensorsForTour = MutableLiveData<List<Sensor>?>(null)
    val sensorsForTour: LiveData<List<Sensor>?>
        get() = _sensorsForTour

    private val interventionRepository = InterventionRepository(database)


    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private var _isNetworkErrorShown = MutableLiveData<Boolean>(false)
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown

    var tourId: Long? = null

    /**
     * Resets the network error flag.
     */
    fun onNetworkErrorShown() {
        _isNetworkErrorShown.value = true
    }

    /**
     * Retrieves the different sensors from the tour.
     */
    fun getSensorsFromTour(tourId: Long) {
        viewModelScope.launch {
            _sensorsForTour.value = sensorRepository.getSensorsFromTour(tourId)
        }
    }

    /**
     * Checks if a sensor is in this specific tour.
     * @param sensorId the sensor to check
     * @return true if it is, false otherwise.
     */
    fun isSensorOnTour(sensorId: Long): Boolean {
        val sensor = sensorsForTour.value?.find { it.idSensor == sensorId }
        return sensor != null
    }


    /**
     * Creates a new intervention for this sensor, with the action "Nothing to do" or equivalent.
     * Marks the sensor as "checked"
     * @param sensorId the sensor to mark
     * @return true if operation successful, false otherwise
     */
    fun markSensorAsGood(sensorId: Long) {
        viewModelScope.launch {
            if(!sensorRepository.isSensorUpdated(sensorId)) {
                val tourSensor = sensorRepository.getTourSensor(tourId, sensorId)
                if(tourSensor != null) {
                    val interventionId = interventionRepository.getNextInterventionId()
                    val intervention = DatabaseIntervention(interventionId, 1L, tourSensor.tourSensorId, "RAS", null)
                    interventionRepository.insertNewIntervention(intervention)
                    sensorRepository.updateSensorWithNothing(sensorId)
                }
            }
        }
    }

    /**
     * Cancel all coroutines when the ViewModel is cleared
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}