package com.viveris.sensorchecker.ui.tourDetails

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.databinding.TourDetailsFragmentBinding
import timber.log.Timber
import java.lang.Exception

class TourDetailsFragment: Fragment() {

    private lateinit var binding: TourDetailsFragmentBinding

    /**
     * Lazy init of the arguments. It's to protect against instantiation that happens before the arguments are passed.
     */
    private val args by lazy {
        TourDetailsFragmentArgs.fromBundle(arguments!!)
    }

    /**
     * Lazy instantiation of the view model, to ensure it is usable.
     */
    private val viewModel: TourDetailsViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(this, TourDetailsViewModelFactory(activity.application))
            .get(TourDetailsViewModel::class.java)
    }

    private var sensorListAdapter: SensorListAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //Sets up the observer for the sensor list, to update the recycler view
        viewModel.sensorsForTour.observe(viewLifecycleOwner, Observer { sensors ->
            if (sensors != null) {
                sensorListAdapter?.sensors = sensors
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.tour_details_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.tourId = args.tourId
        viewModel.getSensorsFromTour(args.tourId)

        (activity as AppCompatActivity).supportActionBar?.title = "Details for tour n°${args.tourId}"
        sensorListAdapter = SensorListAdapter(SensorClick {
            this.findNavController()
                .navigate(TourDetailsFragmentDirections
                    .actionTourDetailsFragmentToInterventionFragment(it.idSensor, args.tourId))
        })

        binding.sensorList.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            layoutManager = LinearLayoutManager(context)
            adapter = sensorListAdapter
            ItemTouchHelper(SwipeCallback(sensorListAdapter!!, viewModel))
                .attachToRecyclerView(this)
        }
        // Observer for the network error.
        viewModel.isNetworkErrorShown.observe(viewLifecycleOwner, Observer<Boolean> { isNetworkError ->
            if (isNetworkError) onNetworkError()
        })

        //Hides the button for qr code scan if the app is not present in the device.
        if(getScanIntent().resolveActivity(activity!!.packageManager) == null) {
            binding.fabScanQr.visibility = View.GONE
        }
        //Launches the QRCode app
        binding.fabScanQr.setOnClickListener {
            try {
                startActivityForResult(getScanIntent(), 0)
            } catch (e: Exception) {
                Timber.d("Error starting activity ${e.message}")
            }
        }

        binding.toursViewModel = viewModel
        return binding.root
    }

    /**
     * Retrieves the result of the scan intent, then tries to send it to the navigation.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0) {
            if(resultCode == RESULT_OK) {
                val sensorId: Int? = data?.getStringExtra("SCAN_RESULT")?.toIntOrNull()
                if (sensorId != null) {
                    if(viewModel.isSensorOnTour(sensorId.toLong())) {
                        this.findNavController().navigate(TourDetailsFragmentDirections.actionTourDetailsFragmentToInterventionFragment(sensorId.toLong(), args.tourId))
                    } else {
                        Snackbar.make(binding.root, "This sensor does not belong to this tour!", Snackbar.LENGTH_LONG)
                            .show()
                    }
                } else {
                    Snackbar.make(binding.root, "Unrecognized sensor", Snackbar.LENGTH_LONG)
                        .show()
                }
            } else if(resultCode == RESULT_CANCELED) {
                Snackbar.make(binding.root, "The QRCode scan has been cancelled", Snackbar.LENGTH_LONG)
                    .show()
            }
        }
    }

    /**
     * Build the scan intent, to scan QR codes
     */
    private fun getScanIntent(): Intent {
        val scanIntent = Intent("com.google.zxing.client.android.SCAN")
        scanIntent.putExtra("SCAN_MODE", "QR_CODE_MODE")
        scanIntent.setPackage("com.google.zxing.client.android")
        return scanIntent
    }

    /**
     * Method for displaying a Toast error message for network errors.
     */
    private fun onNetworkError() {
        if(!viewModel.isNetworkErrorShown.value!!) {
            Toast.makeText(activity, "Offline mode. Using cached data.", Toast.LENGTH_LONG).show()
            viewModel.onNetworkErrorShown()
        }
    }
}