package com.viveris.sensorchecker.ui.interventionList

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.repository.InterventionRepository
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.IOException

class InterventionListViewModel(application: Application): ViewModel() {
    private val interventionRepository = InterventionRepository(SensorCheckerDatabase.getInstance(application))
    val interventions = interventionRepository.interventions

}