package com.viveris.sensorchecker.ui.interventionList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.databinding.InterventionListFragmentBinding
import com.viveris.sensorchecker.databinding.InterventionListItemBinding
import timber.log.Timber

class InterventionListFragment: Fragment() {

    /**
     * Lazy initialization of the viewModel, to ensure the activity is present during the lifecycle.
     */
    private val viewModel: InterventionListViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(this, InterventionListViewModelFactory(activity.application))
            .get(InterventionListViewModel::class.java)
    }

    private var listAdapter: InterventionListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.intervention_list_title)
        val binding: InterventionListFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.intervention_list_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        listAdapter = InterventionListAdapter(InterventionClick {
            Timber.d("Clicked on ${it.idIntervention}")
        })
        binding.interventionsRecyclerView.apply {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
        binding.interventionListVM = viewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.interventions.observe(viewLifecycleOwner, Observer {interventions ->
            interventions?.apply {
                listAdapter?.interventions = interventions
            }
        })
    }
}