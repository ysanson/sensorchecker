package com.viveris.sensorchecker.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.databinding.LoginFragmentBinding
import com.viveris.sensorchecker.utils.hideKeyboard
import timber.log.Timber

class LoginFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding: LoginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        val model = LoginViewModel()
        binding.toursViewModel = model
        binding.lifecycleOwner = this
        (activity as AppCompatActivity).supportActionBar?.title = "Login - SensorChecker"

        model.showSnackBarEvent.observe(viewLifecycleOwner, Observer {
            if (it == true) { // Observed state is true.
                hideKeyboard(binding.root)
                Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    model.snackbarMessage,
                    Snackbar.LENGTH_SHORT // How long to display the message.
                ).show()
                // Reset state to make sure the toast is only shown once, even if the device
                // has a configuration change.
                model.doneShowingSnackbar()
            }
        })

        model.navigateToToursList.observe(viewLifecycleOwner, Observer {
            if(it == true) {
                hideKeyboard(binding.root)
                this.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToToursFragment())
                model.doneNavigating()
            }
        })

        model.tryLogin()

        return binding.root
    }
}