package com.viveris.sensorchecker.ui.tours

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.SensorCheckerApplication
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.databinding.TourListItemBinding
import com.viveris.sensorchecker.domain.Tour
import com.viveris.sensorchecker.repository.TourRepository
import kotlinx.coroutines.*
import timber.log.Timber

/**
 * Adapter for the Tour list recycler view.
 * The layout is from [ToursViewHolder].
 * The callback when an element is clicked is from [TourClick].
 */
class ToursListAdapter(private val callback: TourClick): RecyclerView.Adapter<ToursViewHolder>() {
    /**
     * The elements to show in the recycler view. It is populated in [ToursFragment].
     */
    var tours: List<Tour> = emptyList()
        set(value) {
            field = value
            // Notify any registered observers that the data set has changed. This will cause every
            // element in our RecyclerView to be invalidated.
            notifyDataSetChanged()
        }

    private val context = SensorCheckerApplication.applicationContext()
    private val tourRepository = TourRepository(SensorCheckerDatabase.getInstance(context))

    private val adapterJob = SupervisorJob()
    private val adapterScope = CoroutineScope(adapterJob + Dispatchers.Main)

    /**
     * Creates the viewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToursViewHolder {
        val withDataBinding: TourListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            ToursViewHolder.LAYOUT,
            parent,
            false)
        return ToursViewHolder(withDataBinding)
    }

    override fun getItemCount() = tours.size

    /**
     * Binds the viewHolder and the data.
     * it.tour and it.tourCallback corresponds to the variables declared in the layout.
     */
    override fun onBindViewHolder(holder: ToursViewHolder, position: Int) {
        adapterScope.launch {
            val tour = tours[position]
            val isCompleted =  tourRepository.isTourComplete(tour.idTour)
            val icon = if(isCompleted) context.getDrawable(R.drawable.ic_baseline_check_green_24)
                else context.getDrawable(R.drawable.ic_baseline_build_orange_24)
            holder.viewDataBinding.also {
                it.tour = tour
                it.tourCallback = callback
                it.StatusIcon.setImageDrawable(icon)
            }
        }
    }

    fun getItemAtPosition(position: Int): Tour? {
        return if(position < itemCount){
            tours[position]
        } else null
    }

    fun removeAtPosition(position: Int) = tours.toMutableList().removeAt(position)
}

/**
 * Represents the item in the recycler view.
 */
class ToursViewHolder(val viewDataBinding: TourListItemBinding): RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.tour_list_item
    }
}

/**
 * Takes a function to call back to when a tour is clicked.
 */
class TourClick(val block: (Tour) -> Unit) {
    /**
     * Called when a tour is clicked.
     * @param tour the tour that was clicked.
     * Used in the layout tour_list_item, in the overlay.
     */
    fun onClick(tour: Tour) = block(tour)
}

/**
 * Defines the swipes on the list elements.
 * We do not support moving, hence the false.
 */
class SwipeCallback(private val adapter: ToursListAdapter, private val viewModel: ToursViewModel):
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {

    private var icon: Drawable?
    private var background: ColorDrawable

    init {
        val context = SensorCheckerApplication.applicationContext()
        icon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_cloud_upload_24)
        background = ColorDrawable(ContextCompat.getColor(context, R.color.accent))
    }

    /**
     * Set to false because we don't move list items.
     */
    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder) = false

    /**
     * Defines the action to take when an item is swiped.
     */
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val tour = adapter.getItemAtPosition(viewHolder.adapterPosition)
        if (tour != null) {
            val isDeleted = viewModel.sendForTour(tour.idTour)
            if (isDeleted) adapter.removeAtPosition(viewHolder.adapterPosition)
        }
        adapter.notifyDataSetChanged()
    }

    /**
     * Draws the element, especially the swipes.
     */
    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val itemView = viewHolder.itemView
        val backgroundCornerOffset = 20
        val iconMargin = (itemView.height - icon!!.intrinsicHeight) / 2
        val iconTop = itemView.top + (itemView.height - icon!!.intrinsicHeight) / 2
        val iconBottom = iconTop + icon!!.intrinsicHeight

        when {
            dX > 0 -> { // Swiping to the right
                val iconLeft = itemView.left + iconMargin
                val iconRight = itemView.left + iconMargin + icon!!.intrinsicWidth
                icon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)

                background.setBounds(
                    itemView.left, itemView.top,
                    itemView.left + dX.toInt() + backgroundCornerOffset,
                    itemView.bottom)
            }
            dX < 0 -> { //Swiping to the lest
                val iconLeft = itemView.right - iconMargin - icon!!.intrinsicWidth
                val iconRight = itemView.right - iconMargin
                icon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.setBounds(
                    itemView.right + dX.toInt() - backgroundCornerOffset,
                    itemView.top, itemView.right, itemView.bottom)
            }
            else -> background.setBounds(0, 0, 0, 0)
        }

        background.draw(c)
        icon!!.draw(c)
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

}