package com.viveris.sensorchecker.ui.intervention

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class InterventionViewModelFactory(private val app: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InterventionViewModel::class.java)) {
            return InterventionViewModel(app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}