package com.viveris.sensorchecker.ui.tourDetails

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TourDetailsViewModelFactory(private val app: Application): ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TourDetailsViewModel::class.java)) {
            return TourDetailsViewModel(app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}