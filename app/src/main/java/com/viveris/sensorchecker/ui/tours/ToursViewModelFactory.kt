package com.viveris.sensorchecker.ui.tours

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ToursViewModelFactory(private val app: Application) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ToursViewModel::class.java)) {
            return ToursViewModel(app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}