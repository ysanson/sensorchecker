package com.viveris.sensorchecker.ui.login

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.viveris.sensorchecker.BR
import com.viveris.sensorchecker.SensorCheckerApplication
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import timber.log.Timber
import com.viveris.sensorchecker.network.ForbiddenException
import com.viveris.sensorchecker.network.NoConnectivityException
import com.viveris.sensorchecker.network.UnauthorizedException
import com.viveris.sensorchecker.repository.TokenRepository
import com.viveris.sensorchecker.utils.refreshTokens

/**
 * ViewModel for the login fragment
 */
class LoginViewModel: BaseObservable() {

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)
    private val application by lazy {
        SensorCheckerApplication.applicationContext()
    }

    private val userRepository = UserRepository(SensorCheckerDatabase.getInstance(application))

    @get:Bindable
    var username: String = ""
        set(value) {
            if(field == value) return
            field = value
            notifyPropertyChanged(BR.username)
        }

    @get:Bindable
    var password: String = ""
        set(value) {
            if(field == value) return
            field = value
            notifyPropertyChanged(BR.password)
        }

    /**
     * Request a toast by setting this value to true.
     *
     * This is private because we don't want to expose setting this value to the Fragment.
     */
    private var _showSnackbarEvent = MutableLiveData<Boolean?>()

    /**
     * If this is true, immediately `show()` a toast and call `doneShowingSnackbar()`.
     */
    val showSnackBarEvent: LiveData<Boolean?>
        get() = _showSnackbarEvent

    var snackbarMessage: String = ""

    fun doneShowingSnackbar() {
        _showSnackbarEvent.value = null
    }

    private var _navigateToToursList = MutableLiveData<Boolean?>()
    val navigateToToursList: LiveData<Boolean?>
        get() = _navigateToToursList

    fun doneNavigating() {
        _navigateToToursList.value = null
    }

    fun tryLogin() {
        viewModelScope.launch {
            try {
                refreshTokens()
                val user = userRepository.getUser()
                if (user != null) {
                    _navigateToToursList.value = true
                }
            } catch (e: UnauthorizedException) {
                userRepository.logout()
            } catch (e: NoConnectivityException){
                //If there is a refresh token and a user, we assume he is logged in
                val refreshToken = TokenRepository().getRefreshToken()
                val user = userRepository.getUser()
                if(!refreshToken.isNullOrEmpty() && user!=null)
                    _navigateToToursList.value = true
                else{
                    snackbarMessage = "Not connected and no network. Cannot continue."
                    _showSnackbarEvent.value = true
                }
            }
        }
    }

    /**
     * Controls what is done when the login button is pressed
     */
    fun onPress() {
        viewModelScope.launch {
            try {
                userRepository.authenticate(username, password)
                _navigateToToursList.value = true
            } catch (e: ForbiddenException) {
                userRepository.logout()
                Timber.e(e.fillInStackTrace())
                snackbarMessage = e.message.toString()
                _showSnackbarEvent.value = true
            } catch (e: Exception) {
                Timber.e(e.fillInStackTrace())
                snackbarMessage = e.message.toString()
                _showSnackbarEvent.value = true
            }
        }
    }

    /**
     * Enables the button only if the username and passwords are entered
     */
    val loginButtonEnabled: Boolean
        get() = true
}