package com.viveris.sensorchecker.ui.intervention

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viveris.sensorchecker.database.DatabaseIntervention
import com.viveris.sensorchecker.database.DatabaseSensor
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.domain.Sensor
import com.viveris.sensorchecker.repository.*
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.IOException

class InterventionViewModel(application: Application): ViewModel() {
    private val interventionRepository = InterventionRepository(SensorCheckerDatabase.getInstance(application))

    private val sensorsRepository = SensorRepository(SensorCheckerDatabase.getInstance(application))
    val sensors = sensorsRepository.sensors
    private var _currentSensor = MutableLiveData<Sensor?>(null)
    val currentSensor: LiveData<Sensor?>
        get() = _currentSensor

    private val sensorStateRepository = SensorStateRepository(SensorCheckerDatabase.getInstance(application))
    val states = sensorStateRepository.states

    private val actionRepository = ActionRepository(SensorCheckerDatabase.getInstance(application))
    val actions = actionRepository.actions

    /**
     * Creates the coroutine environment.
     */
    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private var _saveComplete = MutableLiveData(false)
    val saveComplete: LiveData<Boolean>
        get() = _saveComplete


    //Attributes used to create the intervention.
    var isReplaced: Boolean = false

    var sensorId: Long? = null
    var tourId: Long? = null

    var idAction: Long? = null
    var idState: Long? = null

    private var _interventionComments = MutableLiveData("")
    val interventionComments: LiveData<String>
        get() = _interventionComments

    var idNewSensor: Long? = null
    private var tourSensorId: Long = 1L
    private var interventionId: Long = 1L

    /**
     * Gets the necessary IDs in the database.
     * Separate method, because it is launched after the tourId and the sensorId have been initialized.
     */
    fun getIds() {
        if(tourId!= null && sensorId!= null) {
            viewModelScope.launch {
                _currentSensor.value = sensorsRepository.getSensorById(sensorId!!)
                tourSensorId = sensorsRepository.getTourSensor(tourId!!, sensorId!!)?.tourSensorId ?: 1L
                interventionId = interventionRepository.getNextInterventionId()
            }
        }
    }

    fun onCommentsChanged(newText: String) {
        _interventionComments.value = newText
    }

    /**
     * is called when the button "save" is pressed.
     * Creates a new intervention in the database.
     * If the sensor has an updated state, it is updated in the DB as well.
     */
    fun onSaveTriggered() {
        if(sensorId != null && tourId!=null && idAction != null && idState != null) {
            viewModelScope.launch {
                val newIntervention = DatabaseIntervention(
                    interventionId,
                    idAction!!,
                    tourSensorId,
                    _interventionComments.value ?: "",
                    if(isReplaced) idNewSensor else null )

                val sensor = currentSensor.value!!
                val updatedSensor = DatabaseSensor(
                    sensor.idSensor,
                    sensor.idMeasurementType,
                    sensor.reference,
                    sensor.description,
                    sensor.supplier,
                    idState ?: sensor.idState,
                    true)

                sensorsRepository.updateSensor(updatedSensor)
                interventionRepository.insertNewIntervention(newIntervention)
                _saveComplete.value = true
            }
        }
    }

    fun doneSaving() {
        _saveComplete.value = false
    }
}