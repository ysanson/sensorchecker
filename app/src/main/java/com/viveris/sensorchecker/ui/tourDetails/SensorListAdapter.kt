package com.viveris.sensorchecker.ui.tourDetails

import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.SensorCheckerApplication
import com.viveris.sensorchecker.database.SensorCheckerDatabase
import com.viveris.sensorchecker.databinding.SensorListItemBinding
import com.viveris.sensorchecker.domain.Sensor
import com.viveris.sensorchecker.repository.MeasurementTypeRepository
import com.viveris.sensorchecker.repository.SensorRepository
import com.viveris.sensorchecker.repository.SensorStateRepository
import kotlinx.coroutines.*
import timber.log.Timber

/**
 * Adapter for the Recycler View that displays the sensors.
 */
class SensorListAdapter(private val callback: SensorClick): RecyclerView.Adapter<SensorViewHolder>() {
    /**
     * The list of sensors to display.
     */
    var sensors: List<Sensor> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    private val context = SensorCheckerApplication.applicationContext()
    private val database = SensorCheckerDatabase.getInstance(context)
    private val measurementTypeRepository: MeasurementTypeRepository = MeasurementTypeRepository(database)
    private val sensorStateRepository: SensorStateRepository = SensorStateRepository(database)

    private val adapterJob = SupervisorJob()
    private val adapterScope = CoroutineScope(adapterJob + Dispatchers.Main)

    /**
     * Creates the view holder for the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SensorViewHolder {
        val withDataBinding: SensorListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            SensorViewHolder.LAYOUT,
            parent,
            false)
        return SensorViewHolder(withDataBinding)
    }

    override fun getItemCount() = sensors.size

    /**
     * Binds the item with the given data.
     */
    override fun onBindViewHolder(holder: SensorViewHolder, position: Int) {
        adapterScope.launch {
            val sensor = sensors[position]
            val icon = when {
                sensor.isModified -> context.getDrawable(R.drawable.ic_baseline_check_green_24)
                else -> context.getDrawable(R.drawable.ic_baseline_build_orange_24)
            }
            holder.viewDataBinding.also {
                it.sensor = sensor
                it.measurementType = measurementTypeRepository.getMeasurementById(sensor.idMeasurementType)
                it.sensorState = sensorStateRepository.getStateById(sensor.idState)
                it.sensorCallback = callback
                it.stateIcon.setImageDrawable(icon)
            }
        }
    }

    /**
     * Gets a specific sensor at a given position.
     * @param position the position to retrieve the sensor from.
     * @return A sensor, or null if the position id out of bounds.
     */
    fun getItemAtPosition(position: Int): Sensor? {
        return if(position < itemCount){
            sensors[position]
        } else null
    }

    /**
     * Updates the sensor at a given position, then notifies the observer it has changed.
     * This is used to redraw the icon, to match the modified state of the sensor.
     * @param position the position of the sensor to update.
     */
    fun updateItemAtPosition(position: Int) {
        sensors[position].isModified = true
        notifyItemChanged(position)
    }
}

/**
 * Returns the ViewHolder for the recycler view, using the sensor list item layout.
 */
class SensorViewHolder(val viewDataBinding: SensorListItemBinding): RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.sensor_list_item
    }
}

/**
 * Handles the callback when clicking on a sensor.
 */
class SensorClick(val block: (Sensor) -> Unit) {
    fun onClick(sensor: Sensor) = block(sensor)
}


/**
 * Handles the left and right swipes from the sensor item.
 * They are used to mark them as "Nothing to do".
 * Since we are not supporting the moving of items, some methods are not implemented fully.
 */
class SwipeCallback(private val adapter: SensorListAdapter, private val viewModel: TourDetailsViewModel):
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

    private var icon: Drawable?
    private var background: ColorDrawable

    init {
        val context = SensorCheckerApplication.applicationContext()
        icon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_thumb_up_alt_white_24)
        background = ColorDrawable(ContextCompat.getColor(context, R.color.accent))
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder) = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val sensor = adapter.getItemAtPosition(viewHolder.adapterPosition)
        if (sensor != null) {
            viewModel.markSensorAsGood(sensor.idSensor)
            adapter.updateItemAtPosition(viewHolder.adapterPosition)

        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val itemView = viewHolder.itemView
        val backgroundCornerOffset = 20
        val iconMargin = (itemView.height - icon!!.intrinsicHeight) / 2
        val iconTop = itemView.top + (itemView.height - icon!!.intrinsicHeight) / 2
        val iconBottom = iconTop + icon!!.intrinsicHeight

        when {
            dX > 0 -> { // Swiping to the right
                val iconLeft = itemView.left + iconMargin
                val iconRight = itemView.left + iconMargin + icon!!.intrinsicWidth
                icon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)

                background.setBounds(
                    itemView.left, itemView.top,
                    itemView.left + dX.toInt() + backgroundCornerOffset,
                    itemView.bottom)
            }
            dX < 0 -> {
                val iconLeft = itemView.right - iconMargin - icon!!.intrinsicWidth
                val iconRight = itemView.right - iconMargin
                icon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.setBounds(
                    itemView.right + dX.toInt() - backgroundCornerOffset,
                    itemView.top, itemView.right, itemView.bottom)
            }
            else -> background.setBounds(0, 0, 0, 0)
        }

        background.draw(c)
        icon!!.draw(c)
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}