package com.viveris.sensorchecker.ui.interventionList

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class InterventionListViewModelFactory(private val app: Application): ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InterventionListViewModel::class.java)) {
            return InterventionListViewModel(app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}