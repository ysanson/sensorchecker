package com.viveris.sensorchecker.ui.tours

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.viveris.sensorchecker.database.SensorCheckerDatabase.Companion.getInstance
import com.viveris.sensorchecker.domain.User
import com.viveris.sensorchecker.repository.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import java.lang.Exception

class ToursViewModel(application: Application) : ViewModel() {

    private val database by lazy {
        getInstance(application)
    }

    private val toursRepository = TourRepository(database)
    private val sensorRepository = SensorRepository(database)
    private val interventionRepository = InterventionRepository(database)
    private val userRepository = UserRepository(database)

    val tours = toursRepository.tours

    private var _user = MutableLiveData<User?>(null)
    val user: LiveData<User?>
        get() = _user

    /**
     * This is the job for all coroutines started by this ViewModel.
     *
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     *
     * Since we pass viewModelJob, you can cancel all coroutines launched by uiScope by calling
     * viewModelJob.cancel()
     */
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * Event triggered for various reasons. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _showEvent = MutableLiveData<Boolean>(false)

    /**
     * Event triggered for various reasons. Views should use this to get access
     * to the data.
     */
    val showEvent: LiveData<Boolean>
        get() = _showEvent

    /**
     * Flag to display the event message. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _isEventShown = MutableLiveData<Boolean>(false)

    /**
     * Flag to display the event message. Views should use this to get access
     * to the data.
     */
    val isEventShown: LiveData<Boolean>
        get() = _isEventShown

    var eventMessage: String? = "Offline mode. Using cached data."

    private var _isRefreshed = MutableLiveData(false)
    val isRefreshed: LiveData<Boolean>
        get() = _isRefreshed

    init {
        refreshDataFromRepository()
    }

    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    fun refreshDataFromRepository() {
        viewModelScope.launch {
            _isRefreshed.value = false
            try {
                ActionRepository(database).retrieveActions()
                MeasurementTypeRepository(database).refreshMeasurements()
                SensorStateRepository(database).refreshSensorStates()
                toursRepository.refreshTours()
                sensorRepository.refreshSensors()
                _showEvent.value = false
            } catch (networkError: IOException) {
                eventMessage = networkError.message
                _showEvent.value = true
            } finally {
                _isRefreshed.value = true
            }
        }
    }

    /**
     * Sends the information of a specific tour.
     * @param tourId the tour to send the infos.
     * @return boolean, true if successful, false otherwise.
     */
    fun sendForTour(tourId: Long): Boolean {
        var res = false
        viewModelScope.launch {
            try {
                val ints = interventionRepository.sendInterventionsFromTour(tourId)
                val sensors = sensorRepository.sendUpdatedSensorsFromTour(tourId)
                if(ints == 0 || sensors == 0){
                    eventMessage = "No pending operations for tour $tourId"
                    _showEvent.value = true
                }
                else {
                    toursRepository.deleteTourById(tourId)
                    eventMessage = "Sent $ints interventions and $sensors sensors."
                    _showEvent.value = true
                    res = true
                }
            } catch (e: Exception) {
                eventMessage = e.message
                _showEvent.value = true
                Timber.e(e.fillInStackTrace())
            }
        }
        return res
    }

    /**
     * Sends everything currently stored in the cache.
     */
    fun sendPendingOperations(){
        viewModelScope.launch {
            try{
                val ints = interventionRepository.sendNewInterventions()
                val sensors = sensorRepository.sendUpdatedSensors()
                if(ints == 0 || sensors == 0){
                    eventMessage = "No pending operations"
                    _showEvent.value = true
                }
                else {
                    eventMessage = "Sent $ints interventions and $sensors sensors."
                    _showEvent.value = true
                }
            } catch (e: Exception) {
                eventMessage = e.message
                _showEvent.value = true
                Timber.e(e.fillInStackTrace())
            }
        }
    }

    /**
     * Resets the network error flag.
     */
    fun onNetworkErrorShown() {
        _isEventShown.value = false
    }

    fun onDoneRefreshed() {
        _isRefreshed.value = false
    }

    /**
     * Logs out the user.
     */
    fun logout() {
        viewModelScope.launch {
            try{
                UserRepository(database).logout()
            } catch (e: Exception) {
                Timber.e(e.fillInStackTrace())
            }
        }
    }

    /**
     * Gets the current user in the database and puts it in the live data.
     */
    fun getUser() {
        viewModelScope.launch {
            _user.value = userRepository.getUser()
        }
    }

    /**
     * Cancel all coroutines when the ViewModel is cleared
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}
