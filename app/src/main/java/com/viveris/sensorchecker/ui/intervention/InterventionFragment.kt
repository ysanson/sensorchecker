package com.viveris.sensorchecker.ui.intervention

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.viveris.sensorchecker.R
import com.viveris.sensorchecker.databinding.InterventionFragmentBinding
import com.viveris.sensorchecker.domain.Action
import com.viveris.sensorchecker.domain.Sensor
import com.viveris.sensorchecker.domain.SensorState
import com.viveris.sensorchecker.utils.hideKeyboard

class InterventionFragment: Fragment(), AdapterView.OnItemSelectedListener {

    private val viewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProvider(this, InterventionViewModelFactory(activity.application))
            .get(InterventionViewModel::class.java)
    }

    lateinit var binding: InterventionFragmentBinding

    /**
     * Called at the creation of the fragment. Used to create the data binding, and everything related
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.title = "Intervention"
        val args = InterventionFragmentArgs.fromBundle(arguments!!)
        binding = DataBindingUtil.inflate(inflater, R.layout.intervention_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.sensorId = args.sensorId
        viewModel.tourId = args.tourId
        viewModel.getIds()
        setupSpinners()
        setupObservers()

        binding.interventionDesc.text = String.format(getString(R.string.new_intervention_header), args.sensorId.toString())
        binding.interventionViewModel = viewModel
        return binding.root
    }

    /**
     * Setup the different observers used in this screen.
     */
    private fun setupObservers() {
        binding.newSensorSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.newSensorSpinner.visibility = View.VISIBLE
                viewModel.isReplaced = true
            } else {
                binding.newSensorSpinner.visibility = View.GONE
                viewModel.isReplaced = false
            }
        }
        viewModel.currentSensor.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                binding.interventionDesc.text =
                    String.format(getString(R.string.new_intervention_header), it.reference)
            }
        })
        viewModel.saveComplete.observe(viewLifecycleOwner, Observer {
            hideKeyboard(binding.root)
            if (it) {
                val navigated = this.findNavController().popBackStack()
                if (navigated) viewModel.doneSaving()
            }
        })
        binding.commentsTextBox.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.onCommentsChanged(s.toString())
            }

        })
    }

    /**
     * Setup the different spinners used in this view.
     * They are tied to a LiveData, so when content changes, it is automatically updated.
     * Although the lists won't change during an intervention, they are not populated during the creation of the view.
     * That lead to empty spinners.
     */
    private fun setupSpinners() {
        viewModel.states.observe(viewLifecycleOwner, Observer {
            binding.stateSpinner.apply {
                adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, it)
                onItemSelectedListener = this@InterventionFragment
                if(viewModel.currentSensor.value!=null) {
                    setSelection(it.indexOfFirst { state -> state.idState == viewModel.currentSensor.value?.idState })
                }
            }
        })
        viewModel.actions.observe(viewLifecycleOwner, Observer {
            binding.actionSpinner.apply {
                adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, it)
                onItemSelectedListener = this@InterventionFragment
            }
        })
        viewModel.sensors.observe(viewLifecycleOwner, Observer {
            binding.newSensorSpinner.apply {
                adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, it)
                onItemSelectedListener = this@InterventionFragment
            }
        })
    }

    //AdapterView interface implementation for the spinners

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    /**
     * When an item is selected in a spinner, it goes in this listener.
     * It then selects the correct spinner based on the parent ID, and does actions accordingly.
     */
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent != null) {
            when(parent.id) {
                binding.stateSpinner.id -> {
                    val state = parent.selectedItem as SensorState
                    viewModel.idState = state.idState
                    return
                }
                binding.actionSpinner.id -> {
                    val action = parent.selectedItem as Action
                    viewModel.idAction = action.idAction
                    return
                }
                binding.newSensorSpinner.id -> {
                    val sensor = parent.selectedItem as Sensor
                    viewModel.idNewSensor = sensor.idSensor
                    return
                }
                else -> return
            }
        }
    }
}