package com.viveris.sensorchecker

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.viveris.sensorchecker.network.AuthInterceptor
import com.viveris.sensorchecker.network.NetworkUserRequest
import com.viveris.sensorchecker.network.SensorCheckerService
import com.viveris.sensorchecker.network.UnauthorizedInterceptor
import com.viveris.sensorchecker.repository.ITokenRepository
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class NetworkTestsClass {
    private val testJob = SupervisorJob()
    private val testScope = CoroutineScope(testJob + Dispatchers.Main)

    private val tokenRepository = AndroidMockRepo()

    val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val BASE_URL = "http://192.168.1.34:5000"
    var client: OkHttpClient? = null
    var retrofit: Retrofit? = null
    var service: SensorCheckerService? = null

    @Before
    fun setup() {

        client = OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .callTimeout(100, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(AuthInterceptor(tokenRepository, "${BASE_URL}/token"))
            .addInterceptor(UnauthorizedInterceptor())
            .build()

        val moshiBuilder = Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())

        retrofit = Retrofit.Builder()
            .client(client!!)
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshiBuilder.build()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
        service = retrofit!!.create(SensorCheckerService::class.java)
    }


    @Test
    fun loginReturnsNetworkUserResponse(){
        val userId = runBlocking { service?.authenticateAsync(NetworkUserRequest(username = "admin", password = "admin"))?.await()?.userId }
        assert(userId == 1L)
    }

    @Test
    fun afterLogin_ReturnsCorrectlyTheResponse() {
        val tours = runBlocking {
            val response = service?.authenticateAsync(NetworkUserRequest("admin", "admin"))?.await()
            tokenRepository.setTokens(response?.authToken!!, response.refreshToken)
            service?.getToursAsync(1L)?.await()
        }
        assert(tours?.tours?.size!! > 0)
    }


}


class AndroidMockRepo: ITokenRepository {
    private var ac: String? = null
    private var rt: String? = null

    override fun getAccessToken(): String? {
        return ac?: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTkxMDkyMTYxLCJleHAiOjE1OTExNTIxNjF9.0f1i83YPUGVx9duG28PKUFmhZIyG6QISOy3ZUPfzVDY"
    }

    override fun replaceAccessToken(newToken: String) {
        ac = newToken
    }

    override fun removeTokens() {
        ac = null
        rt = null
    }

    override fun getRefreshToken(): String? {
        return rt?:"1.f70179abd6b64055c76dc44eb3cc4e0692a5b4b5bf7b30be8d5b53b17c0ee258916859b2bb2cca5e"
    }

    override fun setTokens(accessToken: String, refreshToken: String) {
        ac = accessToken
        rt = refreshToken
        println("$accessToken / $refreshToken")
    }

}