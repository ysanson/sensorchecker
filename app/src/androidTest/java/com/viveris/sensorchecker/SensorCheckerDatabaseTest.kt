package com.viveris.sensorchecker

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.viveris.sensorchecker.database.*
import org.junit.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*

@RunWith(AndroidJUnit4::class)
class SensorCheckerDatabaseTest {

    private lateinit var userDao: UserDao
    private lateinit var toursDAO: ToursDAO
    private lateinit var db: SensorCheckerDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, SensorCheckerDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        userDao = db.userDao
        toursDAO = db.toursDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertAndRetrieveUser() {
        val user = DatabaseUser(1L, "Yvan", "Sanson")
        userDao.replaceUser(user)
        val storedUser = userDao.getUser()
        assertEquals(storedUser?.idUser, 1L)
    }

    @Test
    fun whenInsertingTour_DateIsNotModified() {
        val currentDate = Date()
        val tour = DatabaseTour(1L, currentDate)
        toursDAO.insertAll(listOf(tour))
        val retrievedTour = toursDAO.getTourById(1L)
        assertEquals(retrievedTour?.tourDate, currentDate)
    }

}