package com.viveris.sensorchecker

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.viveris.sensorchecker.database.*
import com.viveris.sensorchecker.repository.ActionRepository
import com.viveris.sensorchecker.repository.TourRepository
import kotlinx.coroutines.*
import org.junit.After
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RepositoriesTest {

    private lateinit var userDao: UserDao
    private lateinit var toursDAO: ToursDAO
    private lateinit var db: SensorCheckerDatabase
    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + Dispatchers.Main)

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, SensorCheckerDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        userDao = db.userDao
        toursDAO = db.toursDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }


}